from turtle import Turtle
import math
import random
import multiprocessing
import numpy
from csv import DictWriter
import xlsxwriter

POOL_SIZE = 8
NUMBER_OF_TESTS = 100
POPULATION_SIZE = 1000
STARTING_MUTATION_RATE = 1 * (10 ** -6)
MUTATION_DIFFERENCE_MULTIPLIER = 10
SAMPLE_SIZES = [50, 500, 5000]  # [50]#, 100, 150, 200, 250, 300, 350, 400, 450, 500, 2000, 5000]
GENERATIONS_COUNT = 100
EXTINCTION_EVENT_GEN = 50
EXTINCTION_EVENT_DIVIDER = 10
SAMPLE_GEN = None
LIMIT_BY_GENERATIONS = True


def population_has_new_haplotype(population):
    for member in population:
        if member.has_new_haplotype():
            return True
    return False


def population_cycle(population, growth):
    new_population = []
    for member in population:
        number_of_descendants = math.floor(growth)
        chance_of_extra_descendant = growth - math.floor(growth)
        if chance_of_extra_descendant > 0 and random.random() <= chance_of_extra_descendant:
            number_of_descendants += 1
        for _ in xrange(int(number_of_descendants)):
            new_population.append(member.create_decendent())
    return new_population


def get_highest_mutated_member(population):
    return max(population, key=lambda x: x.mt_dna.mutation_percent)


def number_of_gens_untill_different_haplotype(start_size, growth, mutation_rate, sample_size):
    print "Started Simulation!"
    population = []
    for _ in xrange(start_size):
        population.append(Turtle.create_pure_turtle(mutation_rate, sample_size))

    count = 0
    while not population_has_new_haplotype(population):
        population = population_cycle(population, growth)
        count += 1
    print "Took {0} Generations".format(count)
    return count


def get_population_mutation_when_different_haplotype_first_appears(start_size, growth, mutation_rate, sample_size):
    results = {}
    print "Started Simulation! Sample size: {}, Mutation rate: {}".format(sample_size, mutation_rate)
    population = []
    for _ in xrange(start_size):
        population.append(Turtle.create_pure_turtle(mutation_rate, sample_size))

    if LIMIT_BY_GENERATIONS:
        for i in xrange(GENERATIONS_COUNT):
            if SAMPLE_GEN == i:
                results[i] = extract_all_wanted_figuers(population)
            if EXTINCTION_EVENT_GEN == i:
                # results[i] = extract_all_wanted_figuers(population)
                population = extinction_event(population)
                # results[i + 1] = extract_all_wanted_figuers(population)
            else:
                population = population_cycle(population, growth)
            results[GENERATIONS_COUNT] = extract_all_wanted_figuers(population)
        return results


def format_haplotype_results(results):
    lines = []
    sorted_results = sorted(results.items(), key=lambda x: x[0])
    for header, haplotype_lists in sorted_results:
        for i in xrange(len(haplotype_lists)):
            header_string = "{0}.{1}".format(header, i)
            results = ", ".join([str(mutation_percent) for mutation_percent in haplotype_lists[i]])
            lines.append("{0}, {1}".format(header_string, results))
    return "\r\n".join(lines)


def format_mutation_average_and_max_results(results):
    sorted_results = sorted(results.items(), key=lambda x: x[0])
    lines = []
    for header, runs_results in sorted_results:
        cycles = {}
        averages = []
        maximums = []

        for run_result in runs_results:
            for cycle_index in xrange(len(run_result)):
                cycles.setdefault(cycle_index, []).append(run_result[cycle_index])

        sorted_cycles = sorted(cycles.iteritems(), key=lambda x: x[0])

        for cycle, averages_maximums_tuple in sorted_cycles:
            average = sum([single_tuple[0] for single_tuple in averages_maximums_tuple]) / len(averages_maximums_tuple)
            averages.append(str(average))
            maximum = sum([single_tuple[1] for single_tuple in averages_maximums_tuple]) / len(averages_maximums_tuple)
            maximums.append(str(maximum))

        lines.append("{0}, {1}".format(header, ",".join(averages)))
        lines.append("{0}, {1}".format(header, ",".join(maximums)))

    return "\r\n".join(lines)


def haplotypes_distribution(population):
    distro = {}
    for turtle in population:
        haplo = turtle.get_haplotype()
        if haplo not in distro:
            distro[haplo] = 0
        distro[haplo] += 1
    return distro


def extinction_event(population):
    extinction_divide = EXTINCTION_EVENT_DIVIDER
    population = population[:len(population) / extinction_divide]
    new_population = []
    for turtle in population:
        for i in xrange(extinction_divide):
            new_population.append(turtle.create_decendent())
    return new_population


def major_haplotype_score_distribution(population):
    average = 0
    for turtle in population:
        average += turtle.major_haplotype_score()
    average /= float(len(population))
    return average


def extract_all_wanted_figuers(population):
    distro = haplotypes_distribution(population)
    major_haplotype_score = major_haplotype_score_distribution(population)
    hetro_score = hetroplasmic_score(population)
    return {"distro": distro, "major": major_haplotype_score, "hetro_score": hetro_score}


def hetroplasmic_score(population):
    score = {}
    count = {}
    for turtle in population:
        haplo = turtle.get_haplotype()
        if haplo not in score:
            score[haplo] = 0
            count[haplo] = 0
        score[haplo] += turtle.major_haplotype_score()
        count[haplo] += 1
    for haplo in score:
        score[haplo] = score[haplo] / float(count[haplo])
    return score


def average_distro(distros):
    full_distro = {}
    full_count = 0
    for distro in distros:
        for key, value in distro.items():
            if key not in full_distro:
                full_distro[key] = 0
            full_count += value
            full_distro[key] += value
    for key, value in full_distro.items():
        full_distro[key] = value / float(full_count)
    return full_distro


def average_hetroplasmic_score(hetro_score):
    full_score = {}
    full_count = {}
    for res in hetro_score:
        for haplo, score in res.items():
            if haplo not in full_score:
                full_score[haplo] = 0
                full_count[haplo] = 0
            full_score[haplo] += score
            full_count[haplo] += 1
    for haplo in full_score:
        full_score[haplo] /= float(full_count[haplo])
    return full_score


def distros_std(distros):
    full_distro = {}
    distros_count = 0
    for distro in distros:
        for haplotype, score in distro.items():
            if haplotype not in full_distro:
                full_distro[haplotype] = []
            full_distro[haplotype].append(score)
        distros_count += 1
    for haplotype, values in full_distro.items():
        while len(values) < distros_count:
            values.append(0)
    for haplotype, scores in full_distro.items():
        full_distro[haplotype] = numpy.std(scores)
    return full_distro


def average_test_results(results):
    haplo_scores = [result["major"] for result in results]
    average_haplo = sum(haplo_scores) / float(len(results))
    full_distro = average_distro([result["distro"] for result in results])
    distro_std = distros_std([result["distro"] for result in results])
    average_hetroplasmic = average_hetroplasmic_score([result["hetro_score"] for result in results])
    return {"distro": full_distro, "haplo": average_haplo, "hetroplasmic": average_hetroplasmic,
            "distro_std": distro_std}


def format_full_results_lines(results):
    header_row = []
    for sample_size, mutation_rate in sorted(results.keys()):
        header_row.extend([" Sample Size: {}, MutationRate: {}".format(sample_size, mutation_rate), "", ""])
    second_header_row_base = ["Distro", "A", "A'"]
    second_header_row = second_header_row_base * (len(header_row) / len(second_header_row_base))
    formatted_lines = [header_row, second_header_row]
    for sim_index in xrange(len(results.values()[0])):
        lines = {run_index: [] for run_index in xrange(len(results.values()[0]))}
        for sample_size, mutation_rate in sorted(results.keys()):
            expr = results[(sample_size, mutation_rate)]
            for run_index in xrange(len(expr)):
                v = expr[run_index]
                dist = 0
                if "A" in v["distro"]:
                    dist = v["distro"]['A']
                hetro = v["hetro_score"]
                lines[run_index].append(str(dist))
                for haplo in second_header_row_base[1:]:
                    if haplo in hetro:
                        lines[run_index].append(hetro[haplo])
                    else:
                        lines[run_index].append("-")
    for line_index in sorted(lines.keys()):
        formatted_lines.append(lines[line_index])
    return formatted_lines


def format_results_lines(result):
    formatted_lines = {}
    max_rows = 0
    for test_case, value in result.items():
        rows = []
        rows.append(value["haplo"])
        for haplo, score in value["hetroplasmic"].items():
            rows.append("{}: {}".format(haplo, score))
        distro = value["distro"]
        distro = distro.items()
        distro.sort(key=lambda x: x[1], reverse=True)
        distro_std = value["distro_std"]
        for haplotype, value in distro:
            rows.append("{}".format(value))
            rows.append("{}".format(distro_std[haplotype]))

        formatted_lines[test_case] = rows
        if max_rows <= len(rows):
            max_rows = len(rows)

    rows = []
    for i in xrange(max_rows):
        row = {}
        for key in formatted_lines:
            if i < len(formatted_lines[key]):
                row[key] = formatted_lines[key][i]
        rows.append(row)
    return rows


def main():
    results = {}
    workers = multiprocessing.Pool(POOL_SIZE)
    for sample_size in SAMPLE_SIZES:
        for mutation_index in xrange(5):
            mutation_rate = STARTING_MUTATION_RATE * (MUTATION_DIFFERENCE_MULTIPLIER ** mutation_index)
            tests = [workers.apply_async(get_population_mutation_when_different_haplotype_first_appears,
                                         (POPULATION_SIZE, Turtle.POPULATION_GROWTH, mutation_rate, sample_size))
                     for _
                     in xrange(NUMBER_OF_TESTS)]
            test_results = {}
            for test in tests:
                result = test.get()
                for gen in result:
                    if gen not in test_results:
                        test_results[gen] = []
                    test_results[gen].append(result[gen])
            results[(sample_size, mutation_rate)] = test_results.values()[0]
            # for gen, gen_results in test_results.items():
            #     gen_results = average_test_results(gen_results)
            #     results["SampleSize: {} MutationRate: {} Gen: {}".format(sample_size, mutation_rate, gen)] = gen_results
            # with open(r"C:\temp\AllResults.csv", 'wb') as f:
            # w = DictWriter(f, results.keys())
            # w.writeheader()
            # formatted_rows = format_results_lines(results)
            # w.writerows(formatted_rows)
    workbook = xlsxwriter.Workbook(r"C:\temp\AllResultsFullWithHetroWith10PercentBottleneck.xlsx")
    worksheet = workbook.add_worksheet()
    formatted_rows = format_full_results_lines(results)
    for row in xrange(len(formatted_rows)):
        for col in xrange(len(formatted_rows[row])):
            worksheet.write(row, col, formatted_rows[row][col])
    workbook.close()

    with open(r"C:\temp\output5.txt", 'wb') as output:
        output.write(str(results))
    return


if __name__ == '__main__':
    main()

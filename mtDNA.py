import random

MUTATE_BACK = True
NO_HETROPLASMIC = False


class MitochondrialDNASite(object):
    def __init__(self, mutation_percent=0):
        self.mutation_percent = mutation_percent

    def duplicate(self, chance_of_mutation, duplicate_percentage=1):
        if NO_HETROPLASMIC:
            return self.no_hetro_duplicate(chance_of_mutation, duplicate_percentage)
        non_mutated = 1 - self.mutation_percent
        if MUTATE_BACK:
            turned_mutated = non_mutated * chance_of_mutation * duplicate_percentage
            turned_back = self.mutation_percent * chance_of_mutation * duplicate_percentage
            self.mutation_percent = self.mutation_percent + turned_mutated - turned_back
        else:
            self.mutation_percent = self.mutation_percent + (non_mutated * chance_of_mutation * duplicate_percentage)

    def no_hetro_duplicate(self, chance_of_mutation, duplicate_percentage=1):
        if random.random() <= chance_of_mutation:
            self.mutation_percent ^= 1  # Flips the mutation

    def sample(self, sample_size):
        mutations_in_sample = 0.0
        if self.mutation_percent == 1 or self.mutation_percent == 0:
            mutations_in_sample = sample_size * self.mutation_percent
        else:
            for _ in xrange(sample_size):
                if random.random() <= self.mutation_percent:
                    mutations_in_sample += 1.0

        return MitochondrialDNASite(mutations_in_sample / sample_size)

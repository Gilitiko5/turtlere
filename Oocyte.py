class Oocyte(object):
    MT_DNA_PER_OOCYTES = 2 * (10 ** 5)

    def __init__(self, mt_dna_sites):
        self.mt_dna_sites = mt_dna_sites

    def sample_mt_dna_sites(self, sample_size):
        mt_dna_sites = {}
        for name, mt_site in self.mt_dna_sites.items():
            mt_dna_sites[name] = mt_site.sample(sample_size)
        return mt_dna_sites

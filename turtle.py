from Oocyte import Oocyte
from mtDNA import MitochondrialDNASite
import math
import random

NO_HETROPLASMIC = False


class Turtle(object):
    POPULATION_GROWTH = 1

    def __init__(self, mt_dna_sites, mutation_rate, sample_size):
        self.mt_dna_sites = mt_dna_sites
        self.mutation_rate = mutation_rate
        self.sample_size = sample_size
        self.number_of_mt_dna_duplications = int(math.ceil(math.log(Oocyte.MT_DNA_PER_OOCYTES / self.sample_size, 2)))
        if NO_HETROPLASMIC:
            self.number_of_mt_dna_duplications = 1

    @classmethod
    def create_pure_turtle(cls, mutation_rate, sample_size):
        pure_sites = {"A": MitochondrialDNASite()}
        return cls(pure_sites, mutation_rate, sample_size)

    def gen_oocyte(self):
        base_oocyte = Oocyte(self.mt_dna_sites)
        new_mt_dna_sites = base_oocyte.sample_mt_dna_sites(self.sample_size)
        current = self.sample_size
        for i in xrange(self.number_of_mt_dna_duplications):
            if i < (self.number_of_mt_dna_duplications - 1):
                for new_mt_site in new_mt_dna_sites.values():
                    new_mt_site.duplicate(self.mutation_rate)
            else:
                partial_dup = self.calculate_partial_duplicate(current, Oocyte.MT_DNA_PER_OOCYTES)
                for new_mt_site in new_mt_dna_sites.values():
                    new_mt_site.duplicate(self.mutation_rate, partial_dup)
            current *= 2

        return Oocyte(new_mt_dna_sites)

    def calculate_partial_duplicate(self, current_amount, target_amount):
        return (target_amount - current_amount) / float(current_amount)

    def create_decendent(self):
        decendent_oocyte = self.gen_oocyte()
        return Turtle(decendent_oocyte.mt_dna_sites, self.mutation_rate, self.sample_size)

    def has_new_haplotype(self):
        for mt_site in self.mt_dna_sites.values():
            if mt_site.mutation_percent >= 0.5:
                return True
        return False

    def get_haplotype(self):
        haplotye_str = ""
        for name, mt_site in sorted(self.mt_dna_sites.items()):
            haplotye_str += name
            if mt_site.mutation_percent > random.random():
                haplotye_str += "'"
        return haplotye_str

    def major_haplotype_score(self):
        major_haplotype_percentage = 1
        for name, site in self.mt_dna_sites.items():
            major_percent = site.mutation_percent
            if site.mutation_percent < 0.5:
                major_percent = 1 - site.mutation_percent
            major_haplotype_percentage *= major_percent
        return major_haplotype_percentage
